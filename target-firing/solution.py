import sys
import math

# strategy: there's no point in switching from shooting 1 ship to another before destroying it
# fully. So the most optimal solution would be to select the "best" ship to destory
# and destroy them 1 by 1
# This requires developing a heuristic for finding what is the "best" ship to destroy

n = int(input())
ships = []
my_damage = 10
my_strength = 5000

for i in range(n):
    _type, hp, armor, damage = input().split()
    hp = int(hp)
    armor = int(armor)
    damage = int(damage)
    ships.append({'t': _type, 'hp':hp, 'armor':armor, 'damage': damage, 'ID': i})

# run heuristic to select a ship to destroy
def select_ship_to_destroy(ships):
    max_points = 0
    ship_to_destroy = {}
    turns_to_destroy_selected = 0
    for ship in ships:
        ship_type_factor = 2 if ship['t'] == "FIGHTER" else 1
        my_damage_to_this_ship = my_damage * ship_type_factor
        my_damage_to_this_ship = max(my_damage_to_this_ship - ship['armor'], 1)

        turns_to_defeat = math.ceil(ship['hp']/my_damage_to_this_ship)

        points = ship['damage'] * 1/turns_to_defeat
        print("Score of selecting ship with ID", ship['ID'], points, file=sys.stderr, flush=True)
        print("Turns to destroy ID", ship['ID'], turns_to_defeat, file=sys.stderr, flush=True)
        if points > max_points:
            max_points = points
            ship_to_destroy = ship
            turns_to_destroy_selected = turns_to_defeat
    return (ship_to_destroy, turns_to_destroy_selected)

while len(ships) > 0 and my_strength >= 0:
    ship_to_destroy, turns_to_destroy_selected = select_ship_to_destroy(ships)
    print("SELECTED SHIP", ship_to_destroy, file=sys.stderr, flush=True)
    print("turns_to_destroy_selected", turns_to_destroy_selected, file=sys.stderr, flush=True)
    for turn in range(turns_to_destroy_selected):
        for ship in ships:
            my_strength -= ship['damage']
    print("my strength after destroying 1 ship", my_strength, file=sys.stderr, flush=True)

    # remove the selected ship from ships
    ships = [ship for ship in ships if not(ship['ID'] == ship_to_destroy['ID'])]
    print("remaining ships", ships, my_strength, file=sys.stderr, flush=True)

if len(ships) == 0:
    print(my_strength)
else:
    print("FLEE")
